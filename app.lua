local lapis = require("lapis")
local json_params = require("lapis.application").json_params

local routes = require("routes")
local utils = require("utils")

local app = lapis.Application()

utils.fixPrint()
utils.fixJsonContentType(app)
utils.addRoutes(app)

for _, route in pairs(routes) do
    route(app)
end

app:before_filter(function(self)
    self.res.headers["Access-Control-Allow-Origin"] = "*"
end)

function app:handle_404()
    return {
        json = {
            error = "Failed to find route",
            data = {
                route = self.req.cmd_url
            }
        },
        status = 404
    }
end

return app
