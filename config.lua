local config = require("lapis.config")

config("production", {
    port = tonumber(os.getenv("PORT")) or 8080,
    num_workers = 4,
    code_cache = "on",
    logging = {
        queries = false,
        requests = false
    }
})

config("development", {
    port = 8080
})