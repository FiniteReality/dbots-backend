return function(app)
    -- GET /users/@me
    app:get("/users/@me", function()
        return {json = {hello = "world"} }
    end)

    -- GET /users/{user.id}
    app:get("/users/:id[%d]", function(req)
        return {json = {hello = "test", stuff = req.params.id} }
    end)

    -- PATCH /users/@me
    app:patch("/users/@me", function()
        return {json = {hello = "world"} }
    end)
end