return function(app)
    app:get("/version", function()
        return {
            json = {
                lapis = require("lapis.version"),
                lua = _VERSION:gsub("^Lua ", ""),
                luajit = jit and jit.version:gsub("^LuaJIT ", "")
            }
        }
    end)
end