local function random()
    return math.random() * 1e4
end

local description =
[[
# Hello there! #
Pollr is a cool bot. It does cool things.

This is a temporary description, of course. It isn't meant to be anything
special.

## License ##
Pollr is not licensed because it is closed source (shock, horror!)

## Misc Testing ##
We support:

[Links](https://google.com/)

![images](http://lorempixel.com/256/256/)

- Bulletpoints
    1. Numbered lists
    2. With cool numbers

`inline code`

> blockquotes
> and stuff

some text

> warn
> big fat warnings

some more text

> error
> oh noes

And, uh, that's about it.
]]

return function(app)
    app:get("/bots/", function(req)
        return {
            json = {
                {id = "1234", avatar = "http://lorempixel.com/256/256/", name = "Pollr", author = {id = 1234567890, name = "abalabahaha", discrim = "9421"}, description = "The world’s best moderation bot.", likes = random(), dislikes = random()},
                {id = "5678", avatar = "http://lorempixel.com/256/256/", name = "Slash", author = {id = 0987654321, name = "FiniteReality", discrim = "5734"}, description = "Obviously better than Pollr.", likes = random(), dislikes = random()},
                {id = "9012", avatar = "http://lorempixel.com/256/256/", name = "R. Danny", author = {id = 1234509876, name = "Danny", discrim = "0007"}, description = "I am a robot. Beep boop.", likes = random(), dislikes = random()},
                {id = "3456", avatar = "http://lorempixel.com/256/256/", name = "rowboat", author = {id = 0987612345, name = "b1nzy", discrim = "1337"}, description = "Gently, I guess.", likes = random(), dislikes = random()},
                {id = "7890", avatar = "http://lorempixel.com/256/256/", name = "(ﾉ◕ヮ◕)ﾉ✧･ﾟ*✧spoo.py✧*･ﾟ✧ヽ(◕ヮ◕)ﾉ", author = {id = 5432167890, name = "spoopy🍡", discrim = "0567"}, description = "You have been spooked by the spooky skilington.", likes = random(), dislikes = random()}
            }
        }
    end)

    app:get("/bots/:id[%d]", function(req)
        return {
            json = {
                name = "Pollr",
                id = req.params.id,
                author = { id = 1234567890, name = "abalabahaha", discrim = "1234" },
                description_short = "The world’s best moderation bot.",
                avatar = "http://lorempixel.com/256/256/",
                online = true,
                likes = random(),
                dislikes = random(),
                source = "https://github.com/abalabahaha/dbots2-frontend",
                guilds = random(),
                tags = {"fun", "moderation", "games"},
                description = description
            }
        }
    end)
    app:get("/bots/:id[%d]/commands", function(req)
        return {
            json = {
                prefixes = {"!", "."},
                categories = {
                    {
                        name = 'Fun',
                        icon = 'videogame_asset',
                        commands = {
                            {icon = 'casino', name = 'roll', description = 'Roll a dice'},
                            {icon = 'help', name = '8ball', description = 'I haz 8 ball'}
                        }
                    },
                    {
                        name = 'Moderation',
                        icon = 'security',
                        description = 'Mod your mods',
                        commands = {
                            {icon = 'gavel', name = 'ban', description = 'Ban somebody'},
                            {icon = 'feedback', name = 'warn', description = 'Warn somebody'}
                        }
                    },
                    {
                        name = 'Music',
                        icon = 'music_note',
                        description = 'Play the sick tunes',
                        commands = {
                            {icon = 'play_arrow', name = 'play', description = 'Play a sick tune'},
                            {icon = 'pause', name = 'pause', description = 'Cut the music'}
                        }
                    }
                }
            }
        }
    end)
end