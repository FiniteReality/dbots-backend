local lfs = require("lfs")

local ret = { }

for file in lfs.dir("./routes") do
    if lfs.attributes("./routes/"..file, "mode") == "file" then
        ret[file] = dofile("./routes/"..file)
    end
end

return ret