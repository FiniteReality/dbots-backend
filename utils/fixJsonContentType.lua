local function wrapHandler(oldHandler)
    return function(ctx)

        local rsp = oldHandler(ctx)

        if rsp.json then
            rsp.content_type = rsp.content_type or "application/json; charset=utf-8"
        end

        return rsp
    end
end


return function(app)
    function app:match(name, path, handler)
        if handler == nil then
            handler = path
            path = name
            name = nil
        end

        handler = wrapHandler(handler)

        self.ordered_routes = self.ordered_routes or {}
        local key

        if name then
            local tuple = self.ordered_routes[name]

            do
                local old_path = tuple and tuple[next(tuple)]
                if old_path and old_path ~= path then
                    error(("named route mismatch (%s != %s)"):format(old_path, path))
                end
            end

            if tuple then
                key = tuple
            else
                tuple = {
                    [route_name] = path
                }
                self.ordered_routes[route_name] = tuple
                key = tuple
            end
        else
            key = path
        end

        if not self[key] then
            table.insert(self.ordered_routes, key)
        end

        self[key] = handler
        self.router = nil
        return handler
    end
end