local oldPrint = print
return function()
    getfenv().print = function(...)
        local targs, nargs = {...}, select('#', ...)
        for i = 1, nargs do
            targs[i] = tostring(targs[i])
        end

        return oldPrint(table.concat(targs, '\t'))
    end
end