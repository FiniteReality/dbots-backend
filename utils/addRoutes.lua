local respond_to = require("lapis.application").respond_to

return function(app)
    function app:patch(name, path, handler)
        if handler == nil then
            handler = path
            path = name
            name = nil
        end

        self.responders = self.responders or {}
        local existing = self.responders[name or path]
        local tbl = {PATCH = handler}

        if existing then
            setmetatable(tbl, {
                __index = function(self, key)
                    if key:match("%u") then
                        return existing
                    end
                end
            })
        end

        local responder = respond_to(tbl)
        self.responders[name or path] = responder
        return self:match(name, path, responder)
    end
end